/*let es = require('elasticsearch').Client({
hosts: [ process.env.ES_ENDPOINT ],
connectionClass: require('http-aws-es')
});
*/
class ElasticSearchServices {
	constructor(elastic) {
		this.elastic = elastic;
	}

	/**
	 * ESSearchCategory: search with a given category, with a given type and date
	 *
	 * @param {string} paramType
	 * @param {string} paramCategory
	 * @param {number} paramResultsNumber
	 * @param {string} paramDate
	 * @param {string} paramTime
	 * @returns {Promise}
	 */
	async ESSearchCategory(
		paramType = 'article',
		paramCategory = 'news',
		paramResultsNumber = 25,
		paramDate,
		paramTime
	) {
		//console.log('Interrogo ES con questi parametri: ', paramType, paramCategory, paramResultsNumber, paramDate, paramTime);
		let mustParameter;

		if (paramCategory !== 'all') {
			mustParameter = [
				{ match: { TYPE: paramType } },
				{ match: { CATEGORY: paramCategory } },
				{ match: { DATA: paramDate } }
			];
		} else {
			mustParameter = [{ match: { TYPE: paramType } }, { match: { DATA: paramDate } }];
		}

		try {
			let results = this.elastic.search({
				index: process.env.AWS_ES_INDEX,
				type: process.env.AWS_ES_TYPE,
				body: {
					size: paramResultsNumber,
					sort: [{ UNIQUE_COUNT: { order: 'desc' } }],
					query: {
						bool: {
							must: mustParameter,
							filter: [{ term: { DATETIME: paramTime } }]
						}
					}
				}
			});

			return await results;
		} catch (error) {
			console.trace(error.message);
			return error;
		}
	}

	/**
	 * ESSearchDateRange: search with a given category, with a given type and date
	 *
	 * @param {string} paramType
	 * @param {string} paramCategory
	 * @param {number} paramResultsNumber
	 * @param {string} paramDateLte
	 * @param {string} paramDateGte
	 * @returns {Promise} array of results
	 */
	async ESSearchDateRange(
		paramType = 'article',
		paramCategory = 'news',
		paramResultsNumber = 25,
		paramDateGte,
		paramDateLte
	) {
		//console.log('Interrogo ES con questi parametri: ', paramType, paramCategory, paramResultsNumber, paramDateGte, paramDateLte);

		let mustParameter = [{ match: { TYPE: paramType } }, { match: { CATEGORY: paramCategory } }];

		/* "range": {"DATA": {"gte": "20180125", "lte": "20180130"}} restituisce:
beauty-> 67533
news -> 67706
music -> 68161
people -> 68144
sport -> 67522
*/
		//console.log(mustParameter);

		try {
			let results = this.elastic.search({
				index: process.env.AWS_ES_INDEX,
				type: process.env.AWS_ES_TYPE,
				body: {
					size: paramResultsNumber,
					sort: [{ UNIQUE_COUNT: { order: 'desc' } }],
					query: {
						bool: {
							must: [
								{
									range: { DATA: { gte: paramDateGte, lte: paramDateLte } }
								},
								mustParameter
							]
						}
					}
				}
			});

			return await results;
		} catch (error) {
			console.trace(error.message);
			return error;
		}
	}

	/**
	 * ESSearchFree: free search in all index
	 *
	 * @param paramText
	 * @param paramResultsNumber
	 * @returns {Promise}
	 */
	async ESSearchFreeText(paramText, paramResultsNumber = 100) {
		//console.log('Interrogo ES con testo libero: ', paramText);

		try {
			let results = this.elastic.search({
				size: paramResultsNumber,
				q: paramText
			});

			return await results;
		} catch (error) {
			console.trace(error.message);
			return error;
		}
	}

	// todo: create documentations ESPing automatically
	async ESPing() {
		try {
			let result = this.elastic.ping({
				requestTimeout: 3000
			});

			return await result;
		} catch (error) {
			console.trace(error.message);
			return error;
		}
	}
}

module.exports = ElasticSearchServices;
