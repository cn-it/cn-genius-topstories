'use strict';

/**
 * create AWS object and update config
 */
const AWS = require('aws-sdk');

AWS.config.update({ region: 'eu-west-1' });
AWS.config.update({
	credentials: new AWS.Credentials(process.env.API_KEY, process.env.API_SECRET)
});

/**
 * import and init common utils
 */

const commonUtils = require('./controller/commonUtils');
const commonUtilsObj = new commonUtils();

/**
 * import main logic files here
 */
const elasticSearchController = require('./controller/ElasticSearchController');

const elasticSearchService = require('./aws/ElasticSearchServices');
const elasticSearchServiceObj = new elasticSearchService(
	require('elasticsearch').Client({
		hosts: [process.env.ES_ENDPOINT],
		connectionClass: require('http-aws-es')
	})
);

/**
 * getArticleTopStories: search in the index the matching object with 'article' type
 *
 * @param event
 * @param context
 * @param callback
 *
 * local test
 * serverless invoke local --function=getArticleTopStories --path testJson/topStories.json
 *
 * curl test
 * curl -H "Content-Type: application/json" -X GET  https://genius.vanityfair.it/develop-topstories/vanity/v1/getArticleTopStories/news
 * curl -H "Content-Type: application/json" -X GET  https://genius.vanityfair.it/production-topstories/vanity/v1/getArticleTopStories/news
 */
module.exports.getArticleTopStories = async (event, context, callback) => {
	const elasticSearchControllerObj = new elasticSearchController(elasticSearchServiceObj, commonUtilsObj);

	let payload = await elasticSearchControllerObj.getTopStories(event, 'article');

	if (payload.statusCode === 200) {
		callback(null, payload);
	} else {
		callback(payload, null);
	}
};

/**
 * getGalleryTopStories: search in the index the matching object with 'gallery' type
 *
 * @param event
 * @param context
 * @param callback
 *
 * local test
 * serverless invoke local --function=getGalleryTopStories --path testJson/topStories.json
 *
 * curl test
 * curl -H "Content-Type: application/json" -X GET  https://genius.vanityfair.it/develop-topstories/vanity/v1/getGalleryTopStories/news
 * curl -H "Content-Type: application/json" -X GET  https://genius.vanityfair.it/production-topstories/vanity/v1/getGalleryTopStories/news
 */
module.exports.getGalleryTopStories = async (event, context, callback) => {
	const elasticSearchControllerObj = new elasticSearchController(elasticSearchServiceObj, commonUtilsObj);

	let payload = await elasticSearchControllerObj.getTopStories(event, 'gallery');

	if (payload.statusCode === 200) {
		callback(null, payload);
	} else {
		callback(payload, null);
	}
};

/**
 * getVideoTopStories: search in the index the matching object with 'video' type
 *
 * @param event
 * @param context
 * @param callback
 *
 * local test
 * serverless invoke local --function=getVideoTopStories --path testJson/topStories.json
 *
 * curl test
 * curl -H "Content-Type: application/json" -X GET  https://genius.vanityfair.it/develop-topstories/vanity/v1/getVideoTopStories/news
 * curl -H "Content-Type: application/json" -X GET  https://genius.vanityfair.it/production-topstories/vanity/v1/getVideoTopStories/news
 */
module.exports.getVideoTopStories = async (event, context, callback) => {
	const elasticSearchControllerObj = new elasticSearchController(elasticSearchServiceObj, commonUtilsObj);

	let payload = await elasticSearchControllerObj.getTopStories(event, 'video');

	if (payload.statusCode === 200) {
		callback(null, payload);
	} else {
		callback(payload, null);
	}
};

/**
 * getTopStoriesRange: search in the last XXX days the top story for every given channel
 *
 * @param event
 * @param context
 * @param callback
 *
 * local test
 * serverless invoke local --function=getTopStoriesDateRange --path testJson/rangeStories.json
 *
 * curl test
 * curl -H "Content-Type: application/json" -X GET  https://genius.vanityfair.it/develop-topstories/vanity/v1/getTopStoriesDateRange/news
 */
module.exports.getTopStoriesDateRange = async (event, context, callback) => {
	const elasticSearchControllerObj = new elasticSearchController(elasticSearchServiceObj, commonUtilsObj);

	let payload = await elasticSearchControllerObj.getTopStoriesDateRange(event, 'article');

	console.log(payload);
	if (payload.statusCode === 200) {
		callback(null, payload);
	} else {
		callback(payload, null);
	}
};

/**
 * topStoriesSearch: fulltext search in the index
 *
 * @param event
 * @param context
 * @param callback
 *
 * local test
 * serverless invoke local --function=topStoriesSearch --path testJson/freeSearch.json
 */
module.exports.topStoriesSearch = async (event, context, callback) => {
	const elasticSearchControllerObj = new elasticSearchController(elasticSearchServiceObj, commonUtilsObj);

	let payload = await elasticSearchControllerObj.searchInTopStories(event);

	if (payload.statusCode === 200) {
		callback(null, payload);
	} else {
		callback(payload, null);
	}
};

/**
 * ping: Ping ES to check if it's alive
 *
 * @param event
 * @param context
 * @param callback
 *
 * local test
 * serverless invoke local --function=ping
 */
module.exports.ping = async (event, context, callback) => {
	const elasticSearchControllerObj = new elasticSearchController(elasticSearchServiceObj, commonUtilsObj);

	let payload = await elasticSearchControllerObj.ping();

	if (payload.statusCode === 200) {
		callback(null, payload);
	} else {
		callback(payload, null);
	}
};
