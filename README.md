      _____                _  __    _   _           _
     / ____|              | | \_\  | \ | |         | |
    | |     ___  _ __   __| | ___  |  \| | __ _ ___| |_
    | |    / _ \| '_ \ / _` |/ _ \ | . ` |/ _` / __| __|
    | |___| (_) | | | | (_| |  __/ | |\  | (_| \__ \ |_
     \_____\___/|_| |_|\__,_|\___| |_| \_|\__,_|___/\__|


## Condè Nast Genius Top Stories API


### getArticleTopStories
return a JSON with the best articles of the previous hour.

### getGalleryTopStories
return a JSON with the best gallery of the previous hour.

### getVideoTopStories
return a JSON with the best video of the previous hour.

### getTopStoriesDateRange
search in a range of dates

### ping
ping the ESService, return ok if it's alive.


---

### Cache
To activate cache, we need to enable it on each variable we need to cache and in the config of the stage, eg:

https://eu-west-1.console.aws.amazon.com/apigateway/home?region=eu-west-1#/apis/4lzs1ukv0e/stages/production

