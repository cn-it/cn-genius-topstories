'use strict';

class commonUtils {
	/**
	 * s3Controller constructor
	 */
	constructor() {}

	/**
	 * extendReplaceProperties
	 *
	 * receive and overwrite/extend a json with the second optional set of parameters
	 *
	 * @param {json} inputProperties
	 * @returns {*|{}}
	 */
	extendReplaceProperties(inputProperties) {
		inputProperties = inputProperties || {};
		for (let i = 1; i < arguments.length; i++) {
			if (!arguments[i]) continue;
			for (let key in arguments[i]) {
				if (arguments[i].hasOwnProperty(key)) inputProperties[key] = arguments[i][key];
			}
		}

		return inputProperties;
	}

	/**
	 * prepareResponse
	 *
	 * return the json based on the given statusCode parameter
	 *
	 * @param statusCode string
	 * @param responseJson json
	 * @returns {{}}
	 */
	prepareResponse(statusCode, responseJson) {
		let response = {};
		if ('error' === statusCode) {
			console.log('orrore, sono in errore!');
			response = {
				statusCode: 500,
				body: process.env.STAGE === 'develop' ? responseJson : JSON.stringify(responseJson)
			};
		} else {
			response = {
				statusCode: 200,
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json; charset=UTF-8'
				},
				body: process.env.STAGE === 'develop' ? responseJson : JSON.stringify(responseJson)
			};
		}
		return response;
	}

	/**
	 * log
	 *
	 * log info, with some formatting behaviour
	 *
	 * @param {string} infoMessage
	 * @param {string || {}} obj
	 * @returns {{}}
	 */
	log(infoMessage, obj) {
		if (undefined !== obj) {
			console.log('\n');
			console.log(infoMessage, '->', obj);
		} else {
			console.log('\n');
			console.log(infoMessage);
		}
	}

	/* EXAMPLE
    const test = ['aa','bb', 'cc'];

    console.log(concatArray(', ', test, 'dd'));

    return -> aa,bb,cc, dd
    */
	// todo: documentation concatWords
	/**
	 *
	 * @param divider
	 * @param words
	 * @returns {string}
	 */
	concatWords(divider, ...words) {
		return words.join(divider);
	}

	/* EXAMPLE
    const test = ['aa','bb', 'cc'];

    console.log(concatWords(', ', test, 'dd'));

    return -> aa, bb, cc
    */
	// todo: documentation concatArray
	concatArray(divider, words) {
		return words.join(divider);
	}
}

module.exports = commonUtils;
