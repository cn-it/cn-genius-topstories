'use strict';

/**
 * elasticSearchController constructor
 *
 * @param {commonUtils} commonUtils
 * @param lambdaCallback
 */
class elasticSearchController {
	constructor(elasticSearchServiceObj, commonUtils, lambdaCallback) {
		this.elasticSearchServiceObj = elasticSearchServiceObj;
		this.callback = lambdaCallback;
		this.commonUtils = commonUtils;
		this.logs = [];
		this.packResult = this._packResult;
	}

	/**
	 * getTopStories: common method to search in the index the matching object with a given paramCategory type
	 *
	 * @param {event} event
	 * @param {string} paramType used to diversificate the type of object searched: article - gallery - video
	 */
	async getTopStories(event, paramType) {
		/* default variables */
		let localParams = {
			paramType: paramType,
			paramCategory: 'news',
			paramResultsNumber: 25,
			paramDate: undefined,
			paramTime: undefined
		};

		this.commonUtils.extendReplaceProperties(localParams, event.pathParameters);

		setDateAndHour(localParams);

		//console.log('parametri di ricerca: ', today, today.getHours(), paramCategory, paramResultsNumber, paramDate, paramTime);

		try {
			let results = await this.elasticSearchServiceObj.ESSearchCategory(
				localParams.paramType,
				localParams.paramCategory,
				localParams.paramResultsNumber,
				localParams.paramDate,
				localParams.paramTime
			);

			const hits = results.hits.hits;
			const hitsFound = results.hits.total;
			const len = hits.length;

			let resultJson = [];

			// lets build the output json
			resultJson = this._packResult(len, hits, resultJson);

			resultJson = resultJson.filter(
				(item, index, self) =>
					self.findIndex(t => {
						return t.id === item.id;
					}) === index
			);

			const prepareResponseJson = {
				result: resultJson,
				hitsFound: hitsFound,
				status: 'success'
			};

			return this.commonUtils.prepareResponse(prepareResponseJson.status, prepareResponseJson);
		} catch (error) {
			console.log(error);
			const prepareResponseJson = {
				error: error,
				status: 'error'
			};

			return this.commonUtils.prepareResponse(prepareResponseJson.status, prepareResponseJson);
		}
	}

	/**
	 * getTopStories: common method to search in the index the matching object with a given category type
	 *
	 * @param {event} event
	 * @param {string} paramType used to diversify the type of searched object: article - gallery - video
	 */
	async getTopStoriesDateRange(event, paramType) {
		/* default variables */
		let localParams = {
			paramType: paramType,
			paramCategories: 'news',
			paramResultsNumber: 1,
			paramDateLte: undefined,
			paramDateGte: undefined
		};
		//console.log('*** event.pathParameters', event.pathParameters);

		this.commonUtils.extendReplaceProperties(localParams, event.pathParameters);

		// fallback: if no range date, then search in the last five days
		// check if paramDateLte is defined or valid
		if (!localParams.paramDateLte || 'string' === typeof localParams.paramDateLte) {
			localParams.paramDateLte = Date.now();
		}

		// check if paramDateGte is defined or valid
		if (!localParams.paramDateGte || 'string' === typeof localParams.paramDateGte) {
			let fiveAgo = new Date();
			fiveAgo.setDate(fiveAgo.getDate() - 5);
			localParams.paramDateGte = fiveAgo.getTime();
		}

		localParams.paramDateLte = formatDateForES(localParams.paramDateLte);
		localParams.paramDateGte = formatDateForES(localParams.paramDateGte);

		let resultJson = [];

		let getMyPromise = async (targetCategory, ESService, packResults) => {
			try {
				let results = await ESService.ESSearchDateRange(
					localParams.paramType,
					targetCategory,
					localParams.paramResultsNumber,
					localParams.paramDateGte,
					localParams.paramDateLte
				);

				const firstElement = results.hits.hits;
				firstElement.length = 1;

				return packResults(1, firstElement, resultJson);
			} catch (error) {
				console.trace(error.message);
				return '';
			}
		};

		// convert string to array of string, so we can loop on each element
		if ('string' === typeof localParams.paramCategories) {
			localParams.paramCategories = localParams.paramCategories.split(',');
		}

		//console.log('parametri di ricerca: ', localParams, localParams.paramType, localParams.paramCategories, localParams.paramResultsNumber,localParams.paramDateLte, localParams.paramDateGte);

		// cycle through all element of array
		const stuffArray = localParams.paramCategories.map(
			async targetCategory => await getMyPromise(targetCategory, this.elasticSearchServiceObj, this.packResult)
		);

		// wait for all promises
		return Promise.all(stuffArray).then(() => {
			const prepareResponseJson = {
				result: resultJson,
				hitsFound: localParams.paramCategories.length,
				status: 'success'
			};
			console.log('finito');

			return this.commonUtils.prepareResponse(prepareResponseJson.status, prepareResponseJson);
		});
	}

	/**
	 * searchInTopStories: fulltext search in the index
	 *
	 * @param {event} event
	 */
	async searchInTopStories(event) {
		//let promise =  new Promise((resolve, reject) => {
		/* default variables */
		let localParams = {
			paramText: 'scarlett johansson',
			paramResultsNumber: 100
		};

		this.commonUtils.extendReplaceProperties(localParams, event.pathParameters);

		try {
			let result = await this.elasticSearchServiceObj.ESSearchFreeText(localParams.paramText);

			const hits = result.hits.hits;
			const hitsFound = result.hits.total;
			const len = hits.length;

			let resultJson = [];

			// reorder from end to start
			hits.sort(function(a, b) {
				return b._source.DATA - a._source.DATA;
			});

			//resultJson = resultJson.filter((item, index, self) => self.findIndex((t) => {return t.id === item.id; }) === index);

			// lets build the output json
			resultJson = this._packResult(len, hits, resultJson);

			const prepareResponseJson = {
				result: resultJson,
				hitsFound: hitsFound,
				status: 'success'
			};

			return this.commonUtils.prepareResponse(prepareResponseJson.status, prepareResponseJson);
		} catch (error) {
			const prepareResponseJson = {
				error: error,
				status: 'error'
			};

			return this.commonUtils.prepareResponse(prepareResponseJson.status, prepareResponseJson);
		}
	}

	/**
	 * Ping
	 *
	 * Ping ES to check if it's alive
	 */
	async ping() {
		try {
			let result = await this.elasticSearchServiceObj.ESPing();

			const prepareResponseJson = {
				response: result,
				status: 'success'
			};

			return this.commonUtils.prepareResponse(prepareResponseJson.status, prepareResponseJson);
		} catch (error) {
			const prepareResponseJson = {
				error: error,
				status: 'error'
			};

			return this.commonUtils.prepareResponse(prepareResponseJson.status, prepareResponseJson);
		}
	}

	/**
	 * private _packResult
	 *
	 * @param {number} len
	 * @param {Object[]} hits
	 * @param {Object[]} resultJson
	 * @returns {Object[]} array
	 * @private
	 */

	_packResult(len, hits, resultJson) {
		for (let i = 0; i < len; i++) {
			const myJson = {
				id: hits[i]._source.ID,
				url: hits[i]._source.URL,
				uniqueCount: hits[i]._source.UNIQUE_COUNT,
				parentCategory: {
					title: hits[i]._source.CATEGORY,
					url: hits[i]._source.CATEGORY_URL || '#'
				},
				category: {
					title: hits[i]._source.SUB_CATEGORY,
					url: hits[i]._source.SUB_CATEGORY_URL || '#'
				},
				title: hits[i]._source.TITLE,
				images: {
					squared: {
						url: hits[i]._source.IMAGE_SQUARED
					},
					landscape: {
						url: hits[i]._source.IMAGE_LANDSCAPE
					},
					portrait: {
						url: hits[i]._source.IMAGE_PORTRAIT
					}
				},
				type: hits[i]._source.TYPE,
				author: {
					display_name: hits[i]._source.AUTHOR,
					url: hits[i]._source.AUTHOR_URL || '#'
				},
				fullDate: hits[i]._source.EXECUTIONTIME,
				date: hits[i]._source.DATA,
				time: hits[i]._source.DATETIME
			};
			if (myJson.author.display_name === null) {
				myJson.author = null;
			}

			resultJson.push(myJson);
		}

		return resultJson;
	}
}

/**
 * setDateAndHour
 *
 * if no date or time, set now in the right layout
 * day: yyyMMdd
 * hour: HH
 *
 * set current date
 * @param {json} localParams
 * @returns {json} json with date && time formatted as required in our ES: yyyMMdd HH
 */
function setDateAndHour(localParams) {
	const today = new Date();
	today.setMinutes(today.getMinutes() + today.getTimezoneOffset()); // - 60);

	/*
     * if there are no optional parameters, then create the default one:
     * today && one hour ago
     */
	if (!localParams.paramDate) {
		let month = today.getMonth() + 1;
		if (month < 10) {
			month = '0' + month;
		}
		let day = today.getDate();
		if (day < 10) {
			day = '0' + day;
		}
		localParams.paramDate = '' + today.getFullYear() + month + day;
	}

	if (!localParams.paramTime) {
		let hours = today.getHours();
		if (hours < 10) {
			hours = '0' + hours;
		}
		localParams.paramTime = hours;
	}
	return localParams;
}

/**
 * formatDateForES
 *
 * if no date or time, set now in the right layout
 * day: yyyMMdd
 *
 * set current date
 * @param {number} inputTimestamp
 * @returns {string} formatted string: yyyMMdd
 */
function formatDateForES(inputTimestamp) {
	let today;

	if (inputTimestamp) {
		today = new Date(inputTimestamp);
	} else {
		today = new Date();
	}

	today.setMinutes(today.getMinutes() + today.getTimezoneOffset()); // - 60);

	let month = today.getMonth() + 1;
	if (month < 10) {
		month = '0' + month;
	}
	let day = today.getDate();
	if (day < 10) {
		day = '0' + day;
	}

	return '' + today.getFullYear() + month + day;
}

module.exports = elasticSearchController;
