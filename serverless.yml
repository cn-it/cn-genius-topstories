service: cn-genius-topstories

provider:
   name: aws
   runtime: nodejs8.10
   stage: ${opt:stage, 'develop'}
   region: eu-west-1

   deploymentBucket:
    name: serverless-framework-deploy

   environment:
    STAGE: ${self:provider.stage}
    API_KEY: "${self:custom.secrets.API_KEY}"
    API_SECRET: "${self:custom.secrets.API_SECRET}"
    ES_ENDPOINT: "${self:custom.secrets.ES_ENDPOINT}"

iamRoleStatements:
    - Effect: Allow
      Action:
        - s3:PutObject
      Resource: "arn:aws:s3:::${self:custom.bucket}/*"

package:
  exclude:
    - secrets.yml

plugins:
  - serverless-delete-loggroups
  - serverless-plugin-aws-alerts

custom:
  stage: ${opt:stage, self:provider.stage}
  secrets: ${file(secrets.yml):${self:custom.stage}}
  bucket: logs.condenast.it

  alerts:
    dashboards: true

    topics:
      ok:
        topic: ${self:service}-${opt:stage, self:provider.stage}-alerts-ok
      insufficientData:
        topic: ${self:service}-${opt:stage, self:provider.stage}-alerts-insufficientData
      alarm:
        topic: ${self:service}-${opt:stage, self:provider.stage}-alerts-alarm
        notifications:
            - protocol: email
              endpoint: pgenta@condenast.it

    definitions:  # these defaults are merged with your definitions
      customAlarm:
        description: 'Genius topStories alarm'
        namespace: 'AWS/Lambda'
        metric: duration
        threshold: 200
        statistic: Average
        period: 300
        evaluationPeriods: 1
        comparisonOperator: GreaterThanOrEqualToThreshold
    alarms:
      - functionThrottles
      - functionErrors
      - functionInvocations
      - functionDuration

functions:
  ping:
    handler: handler.ping
    name: ${self:service}-ping-${self:provider.stage}
    description: Check if ES is alive
    memorySize: 128 # function specific
    events:
      - http:
          path: vanity/v1/ping
          method: get

  getArticleTopStories:
      handler: handler.getArticleTopStories
      name: ${self:service}-getArticleTopStories-${self:provider.stage}
      description: Retrive most viewed articles from last hour (or at any other given time)
      memorySize: 128 # function specific
      alarms:
        - customAlarm
        - name: ${self:service}-getArticleTopStories-${self:provider.stage}-minutes-gte-1
          description: 'Genius TopStories alarm - something wrong in getArticleTopStories'
          namespace: 'AWS/Lambda'
          metric: errors
          threshold: 1
          statistic: Minimum
          period: 60
          evaluationPeriods: 1
          comparisonOperator: GreaterThanOrEqualToThreshold
      events:
        - http:
            path: vanity/v1/getArticleTopStories/{paramCategory}
            method: get
            cors: true
        - http:
            path: vanity/v1/getArticleTopStories/{paramCategory}/{paramResultsNumber}
            method: get
            cors: true
        - http:
            path: vanity/v1/getArticleTopStories/{paramCategory}/{paramResultsNumber}/{paramDate}
            method: get
            cors: true
        - http:
            path: vanity/v1/getArticleTopStories/{paramCategory}/{paramResultsNumber}/{paramDate}/{paramTime}
            method: get
            cors: true

  getTopStoriesDateRange:
      handler: handler.getTopStoriesDateRange
      name: ${self:service}-getTopStoriesDateRange-${self:provider.stage}
      description: Retrive most viewed articles in a range of dates
      memorySize: 128 # function specific
      alarms:
        - customAlarm
        - name: ${self:service}-getTopStoriesDateRange-${self:provider.stage}-minutes-gte-1
          description: 'Genius TopStories alarm - something wrong in getTopStoriesDateRange'
          namespace: 'AWS/Lambda'
          metric: errors
          threshold: 1
          statistic: Minimum
          period: 60
          evaluationPeriods: 1
          comparisonOperator: GreaterThanOrEqualToThreshold
      events:
        - http:
            path: vanity/v1/getTopStoriesDateRange/
            method: post
            cors: true


  getGalleryTopStories:
      handler: handler.getGalleryTopStories
      name: ${self:service}-getGalleryTopStories-${self:provider.stage}
      description: Retrive most viewed galleries from last hour (or at any other given time)
      memorySize: 128 # function specific
      alarms:
        - customAlarm
        - name: ${self:service}-getGalleryTopStories-${self:provider.stage}-minutes-gte-1
          description: 'Genius TopStories alarm - something wrong in getGalleryTopStories'
          namespace: 'AWS/Lambda'
          metric: errors
          threshold: 1
          statistic: Minimum
          period: 60
          evaluationPeriods: 1
          comparisonOperator: GreaterThanOrEqualToThreshold
      events:
        - http:
            path: vanity/v1/getGalleryTopStories/{paramCategory}
            method: get
            cors: true
        - http:
            path: vanity/v1/getGalleryTopStories/{paramCategory}/{paramResultsNumber}
            method: get
            cors: true
        - http:
            path: vanity/v1/getGalleryTopStories/{paramCategory}/{paramResultsNumber}/{paramDate}
            method: get
            cors: true
        - http:
            path: vanity/v1/getGalleryTopStories/{paramCategory}/{paramResultsNumber}/{paramDate}/{paramTime}
            method: get
            cors: true

  getVideoTopStories:
      handler: handler.getVideoTopStories
      name: ${self:service}-getVideoTopStories-${self:provider.stage}
      description: Retrive most viewed videos from last hour (or at any other given time)
      memorySize: 128 # function specific
      alarms:
        - customAlarm
        - name: ${self:service}-getVideoTopStories-${self:provider.stage}-minutes-gte-1
          description: 'Genius TopStories alarm - something wrong in getVideoTopStories'
          namespace: 'AWS/Lambda'
          metric: errors
          threshold: 1
          statistic: Minimum
          period: 60
          evaluationPeriods: 1
          comparisonOperator: GreaterThanOrEqualToThreshold
      events:
        - http:
            path: vanity/v1/getVideoTopStories/{paramCategory}
            method: get
            cors: true
        - http:
            path: vanity/v1/getVideoTopStories/{paramCategory}/{paramResultsNumber}
            method: get
            cors: true
        - http:
            path: vanity/v1/getVideoTopStories/{paramCategory}/{paramResultsNumber}/{paramDate}
            method: get
            cors: true
        - http:
            path: vanity/v1/getVideoTopStories/{paramCategory}/{paramResultsNumber}/{paramDate}/{paramTime}
            method: get
            cors: true

  topStoriesSearch:
      handler: handler.topStoriesSearch
      name: ${self:service}-topStoriesSearch-${self:provider.stage}
      description: Free text search
      memorySize: 128 # function specific
      alarms:
        - customAlarm
        - name: ${self:service}-topStoriesSearch-${self:provider.stage}-minutes-gte-1
          description: 'Genius TopStories alarm - something wrong in topStoriesSearch'
          namespace: 'AWS/Lambda'
          metric: errors
          threshold: 1
          statistic: Minimum
          period: 60
          evaluationPeriods: 1
          comparisonOperator: GreaterThanOrEqualToThreshold
      events:
        - http:
            path: vanity/v1/topStoriesSearch/{paramText}
            method: get
            cors: true

resources:
  Resources:
    ApiGatewayMethodVanityV1GetarticletopstoriesParamcategoryVarGet:
      Properties:
        RequestParameters:
          method.request.path.paramCategory: true
        Integration:
          RequestParameters:
            integration.request.path.paramCategory: method.request.path.paramCategory
          CacheNamespace: ApiGatewayMethodParamcategoryVarGetCacheNS
          CacheKeyParameters:
            - method.request.path.paramCategory

    ApiGatewayMethodVanityV1GetarticletopstoriesParamcategoryVarParamresultsnumberVarParamdateVarParamtimeVarGet:
      Properties:
        RequestParameters:
          method.request.path.paramCategory: true
          method.request.path.paramResultsNumber: true
          method.request.path.paramDate: true
          method.request.path.paramTime: true
        Integration:
          RequestParameters:
            integration.request.path.paramCategory: method.request.path.paramCategory
            integration.request.path.paramResultsNumber: method.request.path.paramResultsNumber
            integration.request.path.paramDate: method.request.path.paramDate
            integration.request.path.paramTime: method.request.path.paramTime
          CacheNamespace: ApiGatewayMethodVanityv1getarticletopstoriesParamcategoryParamresultsnumberParamdateParamtimeVarGetCacheNS
          CacheKeyParameters:
            - method.request.path.paramCategory
            - method.request.path.paramResultsNumber
            - method.request.path.paramDate
            - method.request.path.paramTime

    ApiGatewayMethodVanityV1GetgallerytopstoriesParamcategoryVarParamresultsnumberVarParamdateVarParamtimeVarGet:
      Properties:
        RequestParameters:
          method.request.path.paramCategory: true
          method.request.path.paramResultsNumber: true
          method.request.path.paramDate: true
          method.request.path.paramTime: true
        Integration:
          RequestParameters:
            integration.request.path.paramCategory: method.request.path.paramCategory
            integration.request.path.paramResultsNumber: method.request.path.paramResultsNumber
            integration.request.path.paramDate: method.request.path.paramDate
            integration.request.path.paramTime: method.request.path.paramTime
          CacheNamespace: ApiGatewayMethodVanityv1getgallerytopstoriesParamcategoryParamresultsnumberParamdateParamtimeVarGetCacheNS
          CacheKeyParameters:
            - method.request.path.paramCategory
            - method.request.path.paramResultsNumber
            - method.request.path.paramDate
            - method.request.path.paramTime

    ApiGatewayMethodVanityV1GetvideotopstoriesParamcategoryVarParamresultsnumberVarParamdateVarParamtimeVarGet:
      Properties:
        RequestParameters:
          method.request.path.paramCategory: true
          method.request.path.paramResultsNumber: true
          method.request.path.paramDate: true
          method.request.path.paramTime: true
        Integration:
          RequestParameters:
            integration.request.path.paramCategory: method.request.path.paramCategory
            integration.request.path.paramResultsNumber: method.request.path.paramResultsNumber
            integration.request.path.paramDate: method.request.path.paramDate
            integration.request.path.paramTime: method.request.path.paramTime
          CacheNamespace: ApiGatewayMethodVanityv1getvideotopstoriesParamcategoryParamresultsnumberParamdateParamtimeVarGetCacheNS
          CacheKeyParameters:
            - method.request.path.paramCategory
            - method.request.path.paramResultsNumber
            - method.request.path.paramDate
            - method.request.path.paramTime